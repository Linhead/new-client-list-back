import app from './src/app.js';

async function start() {
  const port = process.env.PORT || 3000;
  app.listen(port, () => console.log(`Server started at port ${port}`));
};

start();


// import express from 'express';
// import bodyParser from 'body-parser';
// import cors from 'cors';

// const app = express();
// app.use(bodyParser.json(), cors());

// app.post("/registration", async (req, res) => {
//   const { email, password } = req.body;
//   try {
//     const user = await userService.addUser(email, password);
//     res.status(201).json(user);
//   } catch (err) {
//     res.status(401).json(err.code);
//   }
// });

// app.post("/login", async (req, res) => {
//   const { email, password } = req.body;
//   try {
//     const user = await userService.authenticate(email, password);
//     res.json(user);
//   } catch (err) {
//     res.status(401).json(err.code);
//   }
// });

// app.post("/recoverPassword", async (req, res) => {
//   const { email } = req.body;
//   try {
//     const result = await userService.recoverPassword(email);
//     res.status(201).json(result);
//   } catch(err) {
//     res.status(400).send(err.code);
//     }
// });

// app.get("/getUser", async (req, res) => {
//   const uid = req.query.uid;
//   try {
//     const result = await userService.getUser(uid);
//     res.status(201).json(result);
//   } catch(err) {
//     res.status(400).send(err.code);
//     }
// });

// app.get("/getClients", async (req, res) => {
//   try {
//     const clients  = await userService.getClients();
//     res.status(201).json(clients);
//   } catch(err) {
//     res.status(400).send(err.code);
//     }
// });


