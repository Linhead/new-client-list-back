import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import {
  registration,
  login,
  recoverPassword,
  getUser,
} from './api/user.js'
import { getClients, addClient, editClient, removeClient } from './api/clients.js';

const app = express();
app.use(bodyParser.json(), cors());

app.post('/registration', registration);
app.post ('/login', login);
app.post('/recoverPassword', recoverPassword);
app.get ('/getUser', getUser);
app.get('/getClients', getClients);
app.post('/addClient', addClient);
app.patch('/editClient', editClient);
app.delete('/removeClient', removeClient);

export default app;