import * as clentsController from '../controller/clients.js'

const getClients = async (req, res) => {
  try {
    const clients  = await clentsController.getClients();
    res.status(200).json(clients);
  } catch(err) {
    res.status(400).send(err.code);
  }
};

const addClient = async (req, res) => {
  const client = req.body;
  try {
    await clentsController.addClient(client);
    res.status(200).send();
  } catch(err) {
    res.status(400).send(err.code);
  }
};

const editClient = async (req, res) => {
  const client = req.body;
  try {
    await clentsController.editClient(client);
    res.status(200).send();
  } catch(err) {
    res.status(400).send(err.code);
  }
};

const removeClient = async (req, res) => {
  const id = req.body;
  try {
    await clentsController.removeClient(id);
    res.status(200).send();
  } catch(err) {
    res.status(400).send(err.code);
  }
};

export { getClients, addClient, editClient, removeClient }