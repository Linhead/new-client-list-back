import * as userController from '../controller/user.js'

const registration = async (req, res) => {
  const {
    email,
    password
  } = req.body;
  try {
    const user = await userController.addUser(email, password);
    res.status(201).json(user);
  } catch (err) {
    res.status(401).json(err.code);
  }
}

const login = async (req, res) => {
  const {
    email,
    password
  } = req.body;
  try {
    const user = await userController.authenticate(email, password);
    res.json(user);
  } catch (err) {
    res.status(401).json(err.code);
  }
}

const recoverPassword = async (req, res) => {
  const {
    email
  } = req.body;
  try {
    const result = await userController.recoverPassword(email);
    res.status(201).json(result);
  } catch (err) {
    res.status(400).send(err.code);
  }
}

const getUser = async (req, res) => {
  const uid = req.query.uid;
  try {
    const result = await userController.getUser(uid);
    res.status(200).json(result);
  } catch (err) {
    res.status(400).send(err.code);
  }
}

export {
  registration,
  login,
  recoverPassword,
  getUser
}