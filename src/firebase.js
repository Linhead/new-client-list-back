import { initializeApp } from 'firebase/app';

const firebase = initializeApp({
  apiKey: "AIzaSyCS9W1wdZcJHUgUa6v0Teqqp49jq66iQEY",
  authDomain: "client-list-7bb1b.firebaseapp.com",
  projectId: "client-list-7bb1b",
  storageBucket: "client-list-7bb1b.appspot.com",
  messagingSenderId: "606879077408",
  appId: "1:606879077408:web:712be03ed9904154ef9845",
  measurementId: "G-553VLCKBP6",
});

export default firebase
