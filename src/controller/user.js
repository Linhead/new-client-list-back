import firebase from "../firebase.js";
import admin from 'firebase-admin';
import dotenv from 'dotenv';
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  sendPasswordResetEmail
} from "firebase/auth";

dotenv.config();

const serviceAccount = JSON.parse(process.env.GOOGLE_CREDS)
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const auth = getAuth(firebase);

const addUser = (email, password) =>
  createUserWithEmailAndPassword(auth, email, password);

const authenticate = (email, password) =>
  signInWithEmailAndPassword(auth, email, password);

const recoverPassword = (email) =>
  sendPasswordResetEmail(auth, email)

const getUser = (uid) => 
  admin.auth().getUser(uid)

export { addUser, authenticate, recoverPassword, getUser} ;