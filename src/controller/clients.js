import firebase from "../firebase.js";
import { getFirestore, collection, getDocs, doc, setDoc, deleteDoc } from "firebase/firestore"; 
const db = getFirestore(firebase);

const getClients = async () => {
  const clientsCol = collection(db, 'clients');
  const clientsSnapshot = await getDocs(clientsCol);
  const clientsList = clientsSnapshot.docs.map(doc => doc.data());
  return clientsList;
}

const addClient = async (client) => {
  const newClientRef = doc(collection(db, "clients"));

  await setDoc(newClientRef, { ...client, id: newClientRef });
}

const editClient = async (client) => {
  await setDoc(doc(db, "clients", client.id), client);
}

const removeClient = async (id) => {
  await deleteDoc(doc(db, "clients", id));
}

export { getClients, addClient, editClient, removeClient }